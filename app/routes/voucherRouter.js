//Khai báo thư viện express
const express = require('express')

//Tạo router
const voucherRouter = express.Router();

//Router get all voucher:
voucherRouter.get('/vouchers', (req, res) => {
    console.log(req.method);
    console.log(`Get all vouchers`);
    res.json({
        message: `Get all vouchers`
    })
})

//Router get voucher by id:
voucherRouter.get('/vouchers/:voucherId', (req, res) =>{
    const voucherId = req.params.voucherId;
    console.log(req.method);
    console.log(`Get voucher by Id: ${voucherId}`);
    res.json({
        message: `Get voucher by Id: ${voucherId}`
    })
})

//Router post voucher(create new voucher)
voucherRouter.post('/vouchers', (req, res) => {
    console.log(req.method);
    console.log(`Create new voucher`);
    res.json({
        message: `Create new voucher`
    });
})

//Router put voucher (update voucher by id)
voucherRouter.put('/vouchers/:voucherId', (req, res) => {
    const voucherId = req.params.voucherId;
    console.log(req.method);
    console.log(`Update voucher by Id`);
    res.json({
        message: `Update voucher by Id: ${voucherId}`
    });
})

//Router delete voucher (delete voucher by Id):
voucherRouter.delete('/vouchers/:voucherId', (req, res) => {
    const voucherId = req.params.voucherId;
    console.log(req.method);
    console.log(`Delete voucher by Id`);
    res.json({
        message: `Delete voucher by Id: ${voucherId}`
    })
})

module.exports = {voucherRouter}